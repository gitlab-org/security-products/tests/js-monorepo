# js-monorepo

Test project demonstrating Secure configuration for scanning monorepos with:

* **Language:** Javascript
* **Package Manager:** npm
* **Framework:** express

This project uses [dynamic child pipelines](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html#dynamic-child-pipelines)
to generate a child pipeline configuration with a secure job for each subdirectory

The child pipeline can be viewed to see security results. Once https://gitlab.com/gitlab-org/gitlab/-/issues/215725
is completed, report results should surface within the parent pipeline as well.

## How to use

Please see the [usage documentation](https://gitlab.com/gitlab-org/security-products/tests/common#how-to-use-a-test-project) for Security Products test projects.

## Supported Security Products Features

| Feature             | Supported                 |
|---------------------|---------------------------|
| SAST                | :white_check_mark: |
| Dependency Scanning | :x: |
| Container Scanning  | :x: |
| DAST                | :x: |
| License Management  | :x: |
